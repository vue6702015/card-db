# Use the official MySQL 8.0 image as the base image
FROM mysql:8.0

# Set the working directory to /docker-entrypoint-initdb.d
WORKDIR /docker-entrypoint-initdb.d

# Copy the SQL script to the working directory
COPY data.sql .

# Set environment variables for MySQL
ENV MYSQL_ROOT_PASSWORD=0000
ENV MYSQL_DATABASE=mydb

# Expose the MySQL port
EXPOSE 3306
CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card_table`
--

DROP TABLE IF EXISTS `card_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `card_table` (
  `password` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `attribute` varchar(45) DEFAULT NULL,
  `level` int DEFAULT NULL,
  `atk` int DEFAULT NULL,
  `def` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `price` int DEFAULT '10',
  `remain` int DEFAULT '100',
  PRIMARY KEY (`password`,`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_table`
--

LOCK TABLES `card_table` WRITE;
/*!40000 ALTER TABLE `card_table` DISABLE KEYS */;
INSERT INTO `card_table` VALUES (32710364,'Crystal Beast Ruby Carbuncle','LIGHT',3,300,'300','https://ms.yugipedia.com//thumb/2/25/CrystalBeastRubyCarbuncle-BLCR-EN-UR-1E.png/300px-CrystalBeastRubyCarbuncle-BLCR-EN-UR-1E.png',10,100),(37440988,'Rainbow Overdragon','LIGHT',12,4000,'0','https://ms.yugipedia.com//thumb/e/ef/RainbowOverdragon-SDCB-EN-SR-1E.png/300px-RainbowOverdragon-SDCB-EN-SR-1E.png',10,100),(69937550,'Crystal Beast Amber Mammoth','EARTH',4,1700,'1600','https://ms.yugipedia.com//thumb/1/12/CrystalBeastAmberMammoth-BLCR-EN-UR-1E.png/300px-CrystalBeastAmberMammoth-BLCR-EN-UR-1E.png',10,100),(84544192,'Ultimate Crystal Rainbow Dragon Overdrive','LIGHT',12,4000,'0','https://ms.yugipedia.com//thumb/b/b0/UltimateCrystalRainbowDragonOverdrive-SDCB-EN-UR-1E.png/410px-UltimateCrystalRainbowDragonOverdrive-SDCB-EN-UR-1E.png',10,100),(88888888,'Ultimate Crystal Rainbow Dragon Overdrive888','LIGHT',12,12,'1','https://ms.yugipedia.com//thumb/b/b0/UltimateCrystalRainbowDragonOverdrive-SDCB-EN-UR-1E.png/410px-UltimateCrystalRainbowDragonOverdrive-SDCB-EN-UR-1E.png',10,200),(95600067,'Crystal Beast Topaz Tiger','EARTH',4,1600,'1000','https://ms.yugipedia.com//thumb/f/f1/CrystalBeastTopazTiger-BLCR-EN-UR-1E.png/300px-CrystalBeastTopazTiger-BLCR-EN-UR-1E.png',10,100);
/*!40000 ALTER TABLE `card_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `idcart` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `numbers` int DEFAULT NULL,
  `money` int DEFAULT NULL,
  PRIMARY KEY (`idcart`),
  KEY `fk_cart_1_idx` (`username`),
  CONSTRAINT `fk_cart_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,'Dean','88888888',7,70),(2,'Dean','88888889',7,700),(3,'Dean','95600067',1,10);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `item` varchar(225) DEFAULT NULL,
  `total_money` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_order_1_idx` (`username`),
  CONSTRAINT `fk_order_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'Dean','a','a','a',100);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Update John','john@update.com','123'),(2,'Dean','dean@example.com','123'),(100,'New Record','This is a new record.',NULL),(8888,'RainbowDragon','RainbowDragon@example.com','123');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-19 18:09:27
